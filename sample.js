import "../components/Button.css";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import Navbar2 from "../components/Navbar2";
import ReactTooltip from "react-tooltip";
import Cookies from "js-cookie";

// import image 
import MembershipIcon from '../assets/images/Home/membershipLogo.svg';
import AboutIcon from '../assets/images/Home/aboutLogo.svg';
import SoodevieLogo from '../assets/images/Home/soodevieLogo.svg';
import CollectionLogo from '../assets/images/Home/collectionLogo.svg'

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      place: "top",
      type: "dark",
      effect: "float",
      condition: false,
    };
  }


  changePlace(place) {
    this.setState({
      place: place,
    });
  }

  changeType(type) {
    this.setState({
      type: type,
    });
  }

  changeEffect(effect) {
    this.setState({
      effect: effect,
    });
  }

  _onClick() {
    this.setState({
      condition: true,
    });
  }

  render() {
    const { place, type, effect} = this.state;
    const cookiesToken = Cookies.get('token');
    return (
      <div className="home-page">
        <Navbar2 />
        <div className="homepage-wrapper">
          <div className="logo-wrapper">
            <img
              className="soodevieLogo"
              src={SoodevieLogo}
              alt=""
              data-tip
              data-for="logo"
            />
            <ReactTooltip
              id="main"
              place={place}
              type={type}
              effect={effect}
              multiline={true}
              style={{ color: "white" }}
            />
            <Link to="/about">
              <img
                className="aboutLogo"
                src={AboutIcon}
                alt=""
                data-tip
                data-for="about"
              />
              <ReactTooltip id="about" type="error">
                <span>About Page</span>
              </ReactTooltip>
            </Link>
            <Link to={typeof cookiesToken != 'undefined' ? '/membership' : '/'}>
              <img
                className="collectionLogo"
                src={CollectionLogo}
                alt=""
                data-tip
                data-for="collection"
              />
              { typeof cookiesToken != 'undefined' && (
                <ReactTooltip id="collection" type="error">
                  <span>Assemblage Page</span>
                </ReactTooltip>
              )}
            </Link>
            <Link to={typeof cookiesToken != 'undefined' ? '/collection' : '/'}>
              <img
                className="membershipLogo"
                src={MembershipIcon}
                alt=""
                data-tip
                data-for="assemblage"
              />
              { typeof cookiesToken != 'undefined' && (
                <ReactTooltip id="assemblage" type="error">
                  <span>Collection Page</span>
                </ReactTooltip>
              )}
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
